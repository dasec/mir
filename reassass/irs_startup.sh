#/bin/bash
# =================================================================
# Master Thesis: Intrusion response strategies
# Author:        Sven Ossenbuehl 
# Script:		 script to configure the IRS at start up (as sudo)
# =================================================================

#configure eth0 (victim network) eth1 (attacker network)
ifconfig eth1 141.100.1.2 netmask 255.255.255.0 arp up
ifconfig eth0 141.100.2.2 netmask 255.255.255.0 arp up

# add routing so that attacker and victim can reach each other 
route add -net 141.100.2.0/24 gw 141.100.2.3
route add -net 141.100.1.0/24 gw 141.100.1.1 

# enable forwarding and add default gateway (as victim IP)
# to route all traffic from any network to the victim
echo 1 > /proc/sys/net/ipv4/ip_forward

iptables -P FORWARD ACCEPT
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT

iptables -A FORWARD -i eth1 -o eth0 -j ACCEPT
iptables -A FORWARD -i eht0 -o eth1 -m state --state RELATED,ESTABLISHED -j ACCEPT

route add default gw 141.100.1.3