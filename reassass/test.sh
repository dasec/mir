#/bin/bash
# =================================================================
# Master Thesis: Intrusion response strategies on anomalous events 
# Author:        Sven Ossenbühl 
# Script:		 script to run a test
# =================================================================

# =================================================================
# variables
# =================================================================
MYUSER=root
MYPASS=test
MYSERVER=localhost
MYDB=snorby
PW=inetsec

BARNYARDCONF=/usr/local/etc/barnyard2.conf
SNORTLOGDIR=/var/log/snort
SNORTPATTERN=merged.log
SNORTPATH=/usr/local/bin/snort

# =================================================================
# barnyard2 reads alert (in unified2 format)
# and saves it to MySQL database
# continuous mode
# barnyard -d log/alert directory
#		   -f file pattern
#		   -c /path/to/barnyard2.config
#		   -n for parsing only new files (when stamps are active)
# =================================================================
barnyard2 -d /var/log/snort/ -f merged.log -c /usr/local/etc/barnyard2.conf -n
