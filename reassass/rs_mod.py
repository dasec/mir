#!/usr/bin/python
# =================================================================
# Master Thesis: Intrusion response strategies on anomalous events 
# Author:        Sven Ossenbuehl 
# Script:	     Response selection module
#				 Python script to parse alert files from Snort
# =================================================================
import sys, getopt 	# for parsing arguments
import commands 	# for system commands

import socket, struct # for converting IP addresses from string to int
					  # and vice versa

def convert_ip_string_to_int(string):
    """
    Convert an IP string to long
    """

    packetIP = socket.inet_aton(string)
    return struct.unpack("!L", packetIP)[0]	

def convert_ip_int_to_string(ipint):
    """
    Convert an IP integer to a string
    """

    return socket.inet_ntoa(struct.pack("!L", ipint))	


import ap_mod		# test

mystring  = " hallo welth! "

print ap_mod.strip(mystring)

ipstring = "192.168.0.1";
print ipstring

ipint =  convert_ip_string_to_int(ipstring)
print type(ipint)
print ipint

print convert_ip_int_to_string(ipint)

#192 474 188 9
#237 214 157 1