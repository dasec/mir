# =================================================================
# Master Thesis: Intrusion response strategies on anomalous events 
# Author:        Sven Ossenbuehl 
# Script:	     Python script to parse alert files from Snort
# =================================================================
import sys, argparse 	# for parsing arguments
import commands 		# for system commands
import unified2.parser  # unified2 parser 
import socket, struct	# convert IP from str to int & vice versa
import datetime			
from time import strftime,strptime

# =================================================================
# GLOBALS
# =================================================================
CL_PATH		= "/etc/nsm/snortBox-eth1/classification.config"
CL_DICT 	= {0 : "Not used"}
SIG_ID_PATH = "/etc/nsm/snortBox-eth1/sid-msg.map"
SIG_ID_DICT = {0: "Not used"}

# =================================================================
# Helper methods
# =================================================================
def strip(string):
	"""
	Method to clear the perpending and appending spaces
	"""
	string = string.lstrip()
	string = string.rstrip()
	return string

def convert_ip_string_to_int(string):
	"""
	Convert an IP string to an integer
	"""
	return struct.unpack("!L", socket.inet_aton(string)[0])

def convert_ip_int_to_string(ipint):
	"""
	Convert an integer representing an IP adress to a string
	"""
	return socket.inet_ntoa( struct.pack("!L", ipint) )

def add_minutes(tm, mins):
	return tm + datetime.timedelta(minutes=mins)

# =================================================================
# SOURCE: http://www.cyberciti.biz/faq/linux-iptables-drop/
# /sbin/iptables -I INPUT -i {INTERFACE-NAME-HERE} -s {IP-HERE} -j DROP
# =================================================================
def block_ip(ip_address, num_min):
	"""
	blocks an IP address for a given amount of minutes.
	"""	

	datestart = datetime.datetime.now()
	datestop  = add_minutes(datestart, num_min)
	
	# iptables has a specific time format YYYY-mm-ddTHH:MM
	# thus we define it with strftime
	start = datestart.strftime("%Y-%m-%dT%H:%M")
	stop  = datestop.strftime("%Y-%m-%dT%H:%M")

	# in test case just print
	print 'iptables -I INPUT -s ' + str(ip_address) + ' -m time --utc --datestart '	+ str(start) + ' --datestop ' + str(stop) + ' -j DROP'

# =================================================================
# How Do I View Blocked IP Address?
# =================================================================
def show_blocked_ips():

	status, output =  commands.getstatusoutput('iptables -L INPUT')
	if (status == 0):
		print output

# =================================================================
# Method to open and parse a file
# =================================================================
def open_file(path):
	f = open(path, "rb")
	
	timestamp = ""
	msg = ""
	classification = ""
	priority = ""
	proto = ""
	srcIP = ""
	scrPort =""
	dstIP = ""
	dstPort = ""

	# early out if file is empty
	num_lines = sum(1 for line in f)
	if (num_lines == 0):
		print "[INFO] inputfile is empty. nothing to do - sorry."
		exit(0)
	else:
		# work around (we have to open the file twice..)
		f = open(path, "rb")
		print "[INFO] processing " + str(num_lines) + " alerts."

		# loop through file
		for line in f:
			anotherstring = line.split("[**]")
			timestamp = anotherstring[0].split(".")[0]
			timestamp = strip(timestamp)

			msg = anotherstring[1].split("]")[1]
			msg = strip(msg)

			tmp = anotherstring[2].split("[")
			classification = (tmp[1])[:-2]

			priority = tmp[2].split("]")[0]
			priority = strip(priority)

			temp = tmp[2].split("}")[0]
			begin = temp.find("{")
			proto = temp[(begin+1):]

			IPandPorts = tmp[2].split("}")[1]
			begin = IPandPorts.find("->")
			srcIPandPort = IPandPorts[1:(begin)]
			srcIPandPort = strip(srcIPandPort)
			dstIPandPort = IPandPorts[(begin+3):]
			dstIPandPort = strip(dstIPandPort)

			srcIP = srcIPandPort.split(":")[0]
			srcPort = srcIPandPort.split(":")[1]

			dstIP = dstIPandPort.split(":")[0]
			dstPort = dstIPandPort.split(":")[1]


# =================================================================
# IANA Assigned Internet Protocol Numbers:
# =================================================================
def get_iph_protocol(num):
	"""
	Returns the protocol as string 	the numbers are according to IANA.org assigned protocol numbers
	"""

	if num == 1:
		return "ICMP"
	elif num == 6:
		return "TCP"
	elif num == 17:
		return "UDP"

def define_classifications():
	"""
	Reads the classification descriptions from Snort (classification.config) and stores it into the CL_DICT dictonary
	"""

	f = open(CL_PATH, "rb")
	# early out if file is empty
	num_lines = sum(1 for line in f)
	if (num_lines == 0):
		print "[ERROR] classiciation file is empty."
		exit(2)
	else:
		# work around (we have to open the file twice..)
		f = open(CL_PATH, "rb")
		counter = 0
		for line in f:
			if counter not in CL_DICT:
				CL_DICT[counter] = line.split(',')[1]
			counter += 1	

def define_sig_identifiers():
	"""
	Reads the signature descriptions from Snort (sid-msg.map) and stores it into the SIG_ID_DICT dictonary
	descriptions appear in the sid-msg.map if and only if the rule defintion contains a reference.
	"""
	f = open(SIG_ID_PATH, "rb")
	# early out if file is empty
	num_lines = sum(1 for line in f)
	if (num_lines == 0):
		print "[ERROR] classiciation file is empty."
		exit(2)
	else:
		# work around (we have to open the file twice..)
		f = open(SIG_ID_PATH, "rb")
		counter = 0
		for line in f:
			if counter not in CL_DICT:
				tmp = line.split('||')
				sid = int(tmp[0])
				msg = strip(line.split('||')[1])
				SIG_ID_DICT[sid] = msg
			counter += 1	

def get_signature_descr(num):
	return SIG_ID_DICT.get(num, "No description available.")

def get_classification(num):
	return CL_DICT.get(num, "No classification available.") 

# =================================================================
def uf2(path):
	"""
	Reads in alerts from Snort defined in unified2 file format.
	"""
	# go through all alerts
	for ev, ev_tail in unified2.parser.parse(path):
		# only take alerts with a source 
		if (not (ev.get('ip_source') is None)):

			# message is the signature description of a defined rule.
			msg     	   = str(get_signature_descr(int(ev.get('signature_id'))))
			classification = str(get_classification(int(ev.get('classification_id'))))
			
			# source of alert
			srcIP   	   = convert_ip_int_to_string(ev.get('ip_source'))
			srcPort 	   = str(ev.get('sport_itype'))

			# target 
			dstIP   	   = convert_ip_int_to_string(ev.get('ip_destination')) 
			dstPort 	   = str(ev.get('dport_icode'))

			protocol 	   = get_iph_protocol(ev.get('protocol'))
			priority 	   = str(ev.get('priority_id'))

	     	#print "[**] " + msg + " [**] " + "[Classification: " + classification + "] " + "[Priority: " + priority + "]" + " {" + protocol + "} " + srcIP + ":" + srcPort + " -> " + dstIP + ":" + dstPort
	     

#Event: {'impact': 0, 
     # 'classification_id': 2, 
     # 'sensor_id': 0, 
     # 'ip_source': 1121824799, 
     # 'dport_icode': 21, 
     # 'event_id': 333, 
     # 'priority_id': 2, 
     # 'protocol': 17, 
     # 'event_microsecond': 690168, 
     # 'signature_revision': 1,
     # 'impact_flag': 0, 
     # 'event_second': 1392843586,
     # 'generator_id': 1,
     # 'ip_destination': 2372141571, 
     # 'signature_id': 100002, 
     # 'sport_itype': 6115, 
     # 'blocked': 0}

# =================================================================
# Main Method
# =================================================================
def main():
	inputfile = ""
	outputfile = ""
	fformat = ""

	# parse arguments :)
	parser = argparse.ArgumentParser(description='Parser for alert files.') 
	parser.add_argument('--format',  help='either text or unified2')
	parser.add_argument('--infile',  help='path to inputfile')
	parser.add_argument('--outfile', help='path to outputfile')
   
	args = parser.parse_args()
   
	fformat = args.format
	inputfile = args.infile
	outputfile = args.outfile  
     
	# MAIN:
	if (fformat == "text"):
		print "do the ascii stuff"
		open_file(inputfile)
	elif (fformat == "unified2"):
		define_classifications()
		define_sig_identifiers()
		#print "do the unified2 stuff"
		#uf2(inputfile)
		block_ip("141.100.1.3", 30)
		show_blocked_ips()
	else:
		print "not all arguments specified appropriately"
		exit(2)

if __name__ == "__main__":
	main()

