# =================================================================
# Master Thesis: Reaction strategies on anomalous events 
# 				 in Computer Networks
# Author:        Sven Ossenbuehl 
# Script:		 Setup for attacker start up (requires sudo)
# =================================================================
ifconfig eth0 141.100.1.3 netmask 255.255.255.0 arp up
route add default gw 141.100.1.2