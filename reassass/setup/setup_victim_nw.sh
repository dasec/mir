# =================================================================
# Master Thesis: Reaction strategies on anomalous events 
# 				 in Computer Networks
# Author:        Sven Ossenbuehl 
# Script:		 Setup for victim start up (requires sudo)
# =================================================================
ifconfig eth0 down

echo 0 > /proc/sys/net/ipv4/tcp_synack_retries

ifconfig eth0 141.100.2.3 netmask 255.255.255.0 arp up

route add default gw 141.100.2.2

