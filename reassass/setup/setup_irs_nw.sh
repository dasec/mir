# =================================================================
# Master Thesis: Reaction strategies on anomalous events 
# 				 in Computer Networks
# Author:        Sven Ossenbuehl 
# Script:		 Setup for IRS start up (requires sudo)
# =================================================================

# configure ethernet interfaces
ifconfig eth0 down
ifconfig eth1 down
ifconfig eth0 141.100.2.2 netmask 255.255.255.0 arp promisc up
ifconfig eth1 141.100.1.2 netmask 255.255.255.0 arp promisc up

# add routing so that attacker and victim can reach each other
route add -net 141.100.2.0/24 gw 141.100.2.3
route add -net 141.100.1.0/24 gw 141.100.1.3
route add default gw 141.100.1.3

# enable forwarding 
echo 1 > /proc/sys/net/ipv4/ip_forward

# flush all iptables settings
iptables -F
iptables -X

# configure firewall
iptables -P FORWARD DROP   # deny all
iptables -P INPUT   ACCEPT
iptables -p OUTPUT  ACCEPT 

# filtering: accept only packets from the correct network address space
# to prevent IP spoofing 
iptables -A FORWARD -i eth0 -o eth1 -s 141.100.2.0/24 -j ACCEPT
iptables -A FORWARD -i eth1 -o eth0 -s 141.100.1.0/24 -j ACCEPT

# create a logging CHAINS 
iptables -N LOGNDROP
iptables -N LOGNLIMIT

# enable the logging with prefix
iptables -A LOGNDROP -j LOG --log-prefix "IRS-BLOCKED: " --log-level 7
iptables -A LOGNLIMIT -j LOG --log-prefix "IRS-LIMIT:" --log-level 7

# after the logging the packets will be discarded
iptables -A LOGNDROP -j DROP
iptables -A LOGNLIMIT -j DROP

# all packets to forward will jump to the LOGGING chains by following commands
# iptables -I FORWARD -i eth1 -s 141.100.1.4 -d 141.100.2.3 -j LOGNDROP

#iptables -I FORWARD -i eth1 -p tcp -m limit --limit 3/second -j LOGNLIMIT