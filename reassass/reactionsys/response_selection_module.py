# =================================================================
# Master Thesis: Reaction strategies on Anomalous Events 
#				 in Computer Networks
# Author:        Sven Ossenbuehl 
# Script:	     Python script to select response
# =================================================================
import sys, os, argparse 				# for parsing arguments
import commands 						# for system commands
import datetime							# for time commands
from   time import strftime,strptime	# for time commands

# my modules
from initiation_module import InitiationModule	
from rsr import rsr						# the respose success rate object

class ResponseSelection:
	def __init__(self, rsps):
		# a map containing response success rates
		self.rsrdb = {}
		# i know this because i looked at the snort classification and the snort rules
		self.highestprio = 3
		# switch the priorites, because 1 is the highest and 3 the lowest..
		self.priochanger = { 1 : 3, 2 : 2 , 3 : 1}
		# responses
		self.responses = rsps
		# service importance
		self.serviceimportance = {'LOW' : 0.25, 'MEDIUM': 0.5, 'HIGH': 1.0}
		
	def initialize(self):
	# fill rsrdb with dummy values:
		for i in range(len(self.responses)):
			name = self.responses[i]
			#init new rsr object:
			self.rsrdb[name] = rsr(name,0,0,0)
		# get rsrs
		try:
			# first ensure there is a file
			open('db.pkl', 'a').close()
			# then open it
			pkl_file = open('db.pkl', 'rb')
			for key in self.responses:
				if 'none' not in key:
					try:
						self.rsrdb[key] = pickle.load(pkl_file)
					except Exception, e:
						return
			pkl_file.close()
		except Exception, e:
			raise

	#def save(self):
	#	# open file for writing
	#	output = open('db.pkl', 'wb')

	#	for key in self.rsrdb:
	#		pickle.dump(self.rsrdb[key], output)
	#	output.close()

	# =================================================================
	# priority of the alert is related with the impact on the system.
	# =================================================================
	def get_attack_impact(self, priority, ascending):
		impact = 0
		if (ascending == True):
			# if the highest priority indicates the biggest impact
			impact = float(float(priority) / float(self.highestprio)) 
		else:
			# if the lowest priorty indicates the biggest impact
			# then we have change the logic
			impact = float(float(self.priochanger[int(priority)]) / float(self.highestprio))
		return float(impact)


	def determine_attacked_service(self, dstIP, dstPort, im):
		HTTP_SERVER_IP = self.get_service_address('HTTP', im)
		if (dstIP == HTTP_SERVER_IP):
			return 'HTTP'
		else:
			return 'UNKNOWN'
		#if (dstPort == 80): #AND (dstIP == HTTP_SERVER_IP):
		#	return 'HTTP'
		#if (dstPort == 22):
		#	return 'SSH'
		#if (dstPort == 25):
		#	return 'STMP'
		#if (dstPort == 53):
		#	return 'DNS'
		#if (dstPort == 110):
		#	return 'POP3'


	# =================================================================
	#
	# =================================================================
	def calc_negative_impact(self, service, response, alert_priority, im):
		# FD (funcitionality degradation)
		FD = 0.0

		#print service
		# service importance (LOW, MEDIUM, HIGH)
		si = self.get_service_importance(service, im).strip()
		#print "test: " + str(self.serviceimportance[si])
		#print si
		if response == 'none':
			# in the case of no response we can derive the impact
			# by the impact defined in the alert.
			# get_attack_impact returns float
			FD = float(self.get_attack_impact(alert_priority, False))
		else:
			# in the case of a response, the impact is defined 
			# in a file, the value can be get by 
			# get_response_impact return float
			#print "respose disruptiveness" + str(float(self.get_response_impact(response)))
			FD = float(self.get_response_impact(response, im))

		# negative impact = (functionality degradation + importance of service) / 2
		return float((FD + self.serviceimportance[si]) / 2)

	def calc_positive_impact(self, response):
		#print "rsr = " + str(self.rsrdb[response].dps) + "/" + str(self.rsrdb[response].dpt)
		if ( self.rsrdb[response].dpt  > 0):
			return float( float(self.rsrdb[response].dps) / float(self.rsrdb[response].dpt) )
		else:
			return float(0)

	# =================================================================
	# helper classes
	# =================================================================
	def get_service_importance(self, service_name,im):
		#im = InitiationModule()
		#im.initialize()
		return im.get_service_importance(service_name)
		
	def get_response_impact(self, response_name,im):
		#im = InitiationModule()
		#im.initialize()
		return im.get_response_impact(response_name)

	def get_service_address(self,service_name,im):
		#im = InitiationModule()
		#im.initialize()
		return im.get_service_address(service_name)

	# =================================================================
	# effe
	# =================================================================
	def calc_ef(self, service, response, priority, im):
		
		#return pos_impact - neg_impact
		return float(self.calc_positive_impact(response) - self.calc_negative_impact(service, response, priority, im))

	def process(self, alert_list):
		im = InitiationModule()
		im.initialize()
	
		for alert in alert_list:
			alert.strip()
			
			#priority,proto,srcIP:srcPort,dstIP:dstPort,timestamp
			tmp = alert.split(',')
			priority = tmp[0]
			proto = tmp[1]
			srcIPandPort = tmp[2]
			dstIPandPort = tmp[3]
			timestamp 	 = tmp[4]
			dstPort =""
			dstIP =""
			srcIP =""
			srcPort =""
			#if (proto == 'ICMP'):
			#	dstIP = dstIPandPort
			#	srcIP = srcIPandPort
			#else:
			dstIP = dstIPandPort.split(':')[0]
			dstPort = dstIPandPort.split(':')[1] 
			srcIP = srcIPandPort.split(':')[0]
			srcPort = srcIPandPort.split(':')[1] 
		
			# determine attacked service
			victim = self.determine_attacked_service(dstIP, dstPort, im)
			
			# effectiveness 
			E = {}
			best_ef = 0.0
			best_r	= "none"
			for response in self.responses:
				E[response] = self.calc_ef(victim, response, priority, im)
				if response.find("none") == -1:
					best_ef = E[response]
					best_r	= response
				else:
					if E[response] > best_ef:
						best_ef = E[response]
						best_r	= response

			# call response deployment module
			CMD = "sudo python response_deployment_module.py --src " + srcIPandPort + " --dst " + dstIPandPort + " --response " + best_r + " --proto " + proto  
			#print CMD
			
			status, output = commands.getstatusoutput(CMD)
			#print status, output
			logprefix = ""
			if (best_r == 'blockip'):
				logprefix = 'BLOCKED'
			elif (best_r == 'ratelimit'):
				logprefix = 'LIMITED'

			CMD = "sudo python post_activity_module.py --src " + srcIPandPort + " --dst " + dstIPandPort + " --response " + logprefix + " --timestamp " + timestamp
			status, output = commands.getstatusoutput(CMD)
			#print CMD
			
# =================================================================
# Main Method
# =================================================================
def main():

	# reopen sys.stdout in unbuffered mode; to flush outputs
	sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)

	im = InitiationModule()
	im.initialize()
	
	rslct = ResponseSelection(im.get_arr_of_responses())
	rslct.initialize()

	# we assume stdin 
	lines = sys.stdin.readlines() 
	#print len(lines)
	for line in lines:
		line = line.strip()
		#print line
		#priority,proto,srcIP:srcPort,dstIP:dstPort,timestamp
		tmp = line.split(',')
		priority = tmp[0]
		proto = tmp[1]
		srcIPandPort = tmp[2]
		dstIPandPort = tmp[3]
		#print dstIPandPort
		timestamp 	 = tmp[4]
		dstPort =""
		dstIP =""
		srcIP =""
		srcPort =""
		#if (proto == 'ICMP'):
		#	dstIP = dstIPandPort
		#	srcIP = srcIPandPort
		#else:
		dstIP = dstIPandPort.split(':')[0]
		dstPort = dstIPandPort.split(':')[1] 
		srcIP = srcIPandPort.split(':')[0]
		srcPort = srcIPandPort.split(':')[1] 
		
		# determine attacked service
		victim = rslct.determine_attacked_service(dstIP, dstPort, im)
		
		# effectiveness 
		E = {}
		best_ef = 0.0
		best_r	= "none"
		for response in rslct.responses:
			E[response] = rslct.calc_ef(victim, response, priority, im)
			if response.find("none") == -1:
				best_ef = E[response]
				best_r	= response
			else:
				if E[response] > best_ef:
					best_ef = E[response]
					best_r	= response
		
		#print E
		
		# call response deployment module
		CMD = "sudo python response_deployment_module.py --src " + srcIPandPort + " --dst " + dstIPandPort + " --response " + best_r + " --proto " + proto  
		print CMD
		
		status, output = commands.getstatusoutput(CMD)
		#print status, output
		logprefix = ""
		if (best_r == 'blockip'):
			logprefix = 'BLOCKED'
		elif (best_r == 'ratelimit'):
			logprefix = 'LIMITED'

		CMD = "sudo python post_activity_module.py --src " + srcIPandPort + " --dst " + dstIPandPort + " --response " + logprefix + " --timestamp " + timestamp
		status, output = commands.getstatusoutput(CMD)
		print CMD

if __name__ == "__main__":
	main()

	# =================================================================
	# Service importance,(significance) is derived from SLA penalty fees.
	# =================================================================
	# def define_service_importance(self):
	# 	# we dont have penalty cost,
	# 	# so we assume these values.
	# 	IMPORTANCE['LOW'] = 0.25
	# 	IMPORTANCE['MEDIUM'] = 0.50
	# 	IMPORTANCE['HIGH'] = 1
