# =================================================================
# Master Thesis: Reaction strategies on Anomalous Events 
#				 in Computer Networks
# Author:        Sven Ossenbuehl 
# Script:	     Python script to check if resonse is successful
# =================================================================
import sys, argparse 				# for parsing arguments
import commands 					# for system commands
import datetime						# for time commands
from time import strftime,strptime	# for time commands
import datetime
import pickle
from rsr import rsr

class PostActivity:
	def __init__(self):
		# a map containing response success rates
		self.rsrdb = {}
		# log chain for dropped packets
		self.logcdrp = "LOGNDROP"
		# log prefix for dropped packets
		self.drpprfx = "BLOCKED"
		# log chain for ratelimited packets
		self.logclmt = "LOGNLIMIT"
		# log prefix for ratelimited packets
		self.lmtprfx = "LIMITED"
		# available responses
		self.responses = ['blockip', 'ratelimit']
		# log path
		self.logpath = "/var/log/iptables.log"

	def initialize(self):
		# fill rsrdb with dummy values:
		for i in range(len(self.responses)):
			name = self.responses[i]
			#init new rsr object:
			self.rsrdb[name] = rsr(name,0,0,0)
		# get rsrs
		try:
			# first ensure there is a file
			open('db.pkl', 'a').close()
			# then open it
			pkl_file = open('db.pkl', 'rb')
			for key in self.responses:
				try:
					self.rsrdb[key] = pickle.load(pkl_file)
				except Exception, e:
					return
			pkl_file.close()
		except Exception, e:
			raise

	def save(self):
		# open file for writing
		output = open('db.pkl', 'wb')

		for key in self.rsrdb:
			pickle.dump(self.rsrdb[key], output)
		output.close()

	def get_current_year(self):
		return datetime.datetime.now().strftime("%Y")

	def convert_alert_time_to_logtime(self,alert_time):
		# alert time format is : MM/dd-hh:mm:ss(.miliseconds?)
		# desired time format is defined in syslog-ng as iso
		# which is : YYYY-MM-ddThh:mm:ss+00:00 (where ever that +00:00 come from..)
		monthday = alert_time.split('-')[0]
		time 	 = alert_time.split('-')[1]

		monthday = monthday.replace('/', '-')	 
		current_year = self.get_current_year()

		# with time[:8] we only choose the 'hh:mm:ss' (wich are 8 chars.)
		ret = str(current_year) + "-" + str(monthday) + 'T' + str(time[:8])
		return ret

# =================================================================
# Main Method
# =================================================================
def main():

	# vars
	dstIP   = ""
	dstPort = "" 
	srcIP   = "" 
	srcPort = ""
	
	# create an object the class:
	pa = PostActivity()
	pa.initialize()

	parser = argparse.ArgumentParser(description='Post-Activity Module.') 
	parser.add_argument('--src',  		help='source IP:source Port')
	parser.add_argument('--dst',  		help='destination IP:destination Port')
	parser.add_argument('--response',	help='either BLOCKED or LIMITED')
	parser.add_argument('--proto',		help='tcp, udp, icmp')
	parser.add_argument('--timestamp', 	help='timestamp of the alert')
	
	args = parser.parse_args()

	response  = args.response   
	logpref   = response
	proto     = args.proto
	timestamp = args.timestamp

	if (not (args.src is None)):	
		src 	 = args.src
		if (':' in src):
			srcIP    = src.split(':')[0]
			srcPort  = src.split(':')[1]
		else:
			srcIP    = src

	if (not (args.dst is None)):
		dst 	 = args.dst
		if (':' in dst):
			dstIP    = dst.split(':')[0]
			dstPort  = dst.split(':')[1]
		else:
			dstIP 	 = dst

	#timestamp = "03/26-01:02:54.144372"
	# convert time correct format
	logtime = pa.convert_alert_time_to_logtime(str(timestamp))
	
	# check for dropped packets
	# due to the applied countermeasures.
	f = open(LOG_PATH, 'rb')
	lines = f.readlines()
	for line in lines:
		print 
		tmp = line.split('+')
		ts = tmp[0].strip()
		#print "ts .. as :" + tmp[0].strip()
		#tsalert = test_with_dates().strip()
		tsalert = logtime
		#sIP = str('SRC=')+'141.100.1.7'
		#sPort = 'SPT=' +'1971'
		#dIP = '141.100.2.3'

		if   ( logprf == "BLOCKED" ):
			response = "blockip"
		elif ( logprf == "LIMITED" ):
			response = "ratelimit"
		else:
			#print "[ERROR] something went wrong."
			exit(2)

		# when timestamp is correct along with log-prefix srcIP, srcPort, destIP and destPort
		# we assume that response was successful.
		if ( (tsalert in tmp[0]) and (logprf in tmp[1]) and (srcIP in tmp[1]) and (dstIP in tmp[1]) ):
			#print "found logging entrie for " +srcIP+ " -> " +dstIP+ " with logprefix " + logprf 
			pa.rsrdb[response].add_dpt()
			pa.rsrdb[response].add_dps()
			pa.rsrdb[response].update_rsr()	
			pa.rsrdb[response].showinfo()		
			break  

	#save rsrlogprf
	pa.save()

if __name__ == "__main__":
	main()
