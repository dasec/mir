# =================================================================
# Master Thesis: Reaction Strategies on Anomalous Events 
#				 in Computer Networks
# Author:        Sven Ossenbuehl 
# Script:	     Python script to parse alert files from Snort
# =================================================================
#!/usr/bin/env python
import sys, os, argparse# for parsing arguments
import commands 		# for system commands
import unified2.parser  # unified2 parser 
import socket, struct	# convert IP from str to int & vice versa
import datetime			
from time import sleep

# my modules
from initiation_module import InitiationModule		
from response_selection_module import ResponseSelection

class AlertParser:
	def convert_ip_string_to_int(self, string):
		"""
		Convert an IP string to an integer
		"""
		return struct.unpack("!L", socket.inet_aton(string)[0]) 

	def convert_ip_int_to_string(self, ipint):
		"""
		Convert an integer representing an IP adress to a string
		"""
		return socket.inet_ntoa( struct.pack("!L", ipint) )

	def strip(self, string):
		"""
		Method to clear the perpending and appending spaces
		"""
		return string.strip()

	# =================================================================
	# IANA Assigned Internet Protocol Numbers:
	# =================================================================
	def get_iph_protocol(self, num):
		"""
		Returns the protocol as string. The numbers are according to IANA.org assigned protocol numbers
		"""

		#print "in get iph protocol"
		if   (num == 1):
			#print "we have an icmp"
			return "ICMP"
		elif (num == 6):
			return "TCP"
		elif (num == 17):
			return "UDP"

	def process_line(self, line):
		anotherstring  = line.split("[**]")
		timestamp 	   = anotherstring[0].split(".")[0]
		timestamp 	   = timestamp.strip()
		timestamp	   = timestamp[:14] #cut off milliseconds

		msg 		   = anotherstring[1].split("]")[1]
		msg 		   = msg.strip()

		tmp 		   = anotherstring[2].split("[")
		classification = (tmp[1])[:-2]

		priority 	   = tmp[2].split("]")[0]
		priority 	   = priority.split(":")[1].strip()
		#print priority# 	   = priority.strip()

		temp 		   = tmp[2].split("}")[0]
		begin 		   = temp.find("{")
		proto 		   = temp[(begin+1):]
		proto 		   = proto.strip()

		IPandPorts     = tmp[2].split("}")[1]

		begin 	       = IPandPorts.find("->")
		srcIPandPort   = IPandPorts[1:(begin)]
		srcIPandPort   = srcIPandPort.strip()

		dstIPandPort   = IPandPorts[(begin+3):]
		dstIPandPort   = dstIPandPort.strip()

		if proto == "ICMP":
			#{ICMP} 126.88.95.201:8 -> 141.100.2.3:0
			srcIP 		   = srcIPandPort
			srcPort        = str(8)
			dstIP 		   = dstIPandPort
			dstPort 	   = str(0)

		else:
			srcIP 		   = srcIPandPort.split(":")[0]
			srcPort 	   = srcIPandPort.split(":")[1]

			dstIP 		   = dstIPandPort.split(":")[0]
			dstPort 	   = dstIPandPort.split(":")[1]

		#print "[**] " + msg + " [**] " + "[Classification: " + classification + "] " + "[Priority: " + priority + "]" + " {" + proto + "} " + srcIP + ":" + srcPort + " -> " + dstIP + ":" + dstPort
		# pipe the out put to the response_selection_module
		string =  priority + "," + proto + "," + srcIP + ":" + srcPort + "," + dstIP + ":" + dstPort + "," + timestamp
		#enforce printing
		#sys.stdout.flush()
		return string
		#print csv_string

	# =================================================================
	# Method to either open and parse a file, or parse stdin
	# =================================================================
	def parse_alert_fast(self, path, isstdin, im):

		rslct = ResponseSelection(im.get_arr_of_responses())
		rslct.initialize()

		f = ""
		fileBytePos = 0
		if (isstdin == False):
			#f = open(path, "rb") # early out if file is empty
			#num_lines = sum(1 for line in f)
			#if (num_lines == 0):
			#	print "[INFO] inputfile is empty. nothing to do - sorry."
			#	exit(0)
			#else:
			# such polling, much bad
			while True:
				alerts = []
				f = open(path, "rb")
				f.seek(fileBytePos)
				lines = f.readlines()
				fileBytePos = f.tell()
				f.close()
				for line in lines:
					alerts.append( self.process_line(line) )
					#self.process_line(line)
				
				# process the thus far collected alerts
				rslct.process(alerts)

				# clear list
				alerts = []
				sleep(0.05)
				
				#print "[INFO] processing " + str(num_lines) + " alerts."

		else:
			#alerts = []
			# stdin
			f = path 
			# parse data
			for line in f:
				#alerts.append( 
				self.process_line(line) #)

			
	# =================================================================
	def uf2(path):
		"""
		Reads in alerts from Snort defined in unified2 file format.
		"""
		fileBytePos = 0

		im = initiation_module()
		im.initialize()

		while True:
			# go through all alerts
			ev, fileBytePos = unified2.parser.c_parse(path, fileBytePos)
			#for ev, fileBytePos in unified2.parser.parse(path, fileBytePos):
			for e in ev:
				# only take alerts with a source 
				if (not (e.get('ip_source') is None)):

					# message is the signature description of a defined rule.
					msg     	   = str(im.get_signature_descr(int(e.get('signature_id'))))
					classification = str(im.get_classification(int(e.get('classification_id'))))
					
					# source of alert
					srcIP   	   = convert_ip_int_to_string(e.get('ip_source'))
					srcPort 	   = str(e.get('sport_itype'))

					# target 
					dstIP   	   = convert_ip_int_to_string(e.get('ip_destination')) 
					dstPort 	   = str(e.get('dport_icode'))

					protocol 	   = get_iph_protocol(e.get('protocol'))
					priority 	   = str(e.get('priority_id'))

			     	print "[**] " + msg + " [**] " + "[Classification: " + classification + "] " + "[Priority: " + priority + "]" + " {" + protocol + "} " + srcIP + ":" + srcPort + " -> " + dstIP + ":" + dstPort

# =================================================================
# Main Method
# =================================================================
def main():

	# reopen sys.stdout in unbuffered mode; to flush outputs
	sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)

	# parse arguments 
	parser = argparse.ArgumentParser(description='Parser for alert files.') 
	parser.add_argument('--format',  help='either text or unified2')
	parser.add_argument('--infile',  help='path to inputfile')
   
	args = parser.parse_args()
    
	fformat = args.format
	inputfile = args.infile
	isstdin = False
	
	if not inputfile:
		#if os.fstat(sys.stdin.fileno()).st_size == 0:
		#	print "[ERROR] no input to process" # early out
		#	exit(2)
		inputfile = sys.stdin.readlines() # READ ALL THE LINES
		isstdin = True 
		
	# get all the config stuff
	im = InitiationModule()
	im.initialize()

	ap = AlertParser()
	if (fformat == "text"):
		#print "[INFO] parsing ASCII format."
		ap.parse_alert_fast(inputfile,isstdin,im)

	elif (fformat == "unified2"):
		#print "[INFO] parsing unified2 format."
		if isstdin == False:
			ap.uf2(inputfile)
		else:
			print "[INFO] unified2 can only parsed from file at the moment."

if __name__ == "__main__":
	main()

# 	def define_classifications():
# 	"""
# 	Reads the classification descriptions from Snort (classification.config) and stores it into the CL_DICT dictonary
# 	"""

# 	f = open(CL_PATH, "rb")
# 	# early out if file is empty
# 	num_lines = sum(1 for line in f)
# 	if (num_lines == 0):
# 		print "[ERROR] classiciation file is empty."
# 		exit(2)
# 	else:
# 		# work around (we have to open the file twice..)
# 		f = open(CL_PATH, "rb")
# 		counter = 0
# 		for line in f:
# 			if counter not in CL_DICT:
# 				CL_DICT[counter] = line.split(',')[1]
# 			counter += 1	

# def define_sig_identifiers():
# 	"""
# 	Reads the signature descriptions from Snort (sid-msg.map) and stores it into the SIG_ID_DICT dictonary
# 	descriptions appear in the sid-msg.map if and only if the rule defintion contains a reference.
# 	"""
# 	f = open(SIG_ID_PATH, "rb")
# 	# early out if file is empty
# 	num_lines = sum(1 for line in f)
# 	if (num_lines == 0):
# 		print "[ERROR] classiciation file is empty."
# 		exit(2)
# 	else:
# 		# work around (we have to open the file twice..)
# 		f = open(SIG_ID_PATH, "rb")
# 		counter = 0
# 		for line in f:
# 			if counter not in CL_DICT:
# 				tmp = line.split('||')
# 				sid = int(tmp[0])
# 				msg = strip(line.split('||')[1])
# 				SIG_ID_DICT[sid] = msg
# 			counter += 1	
