# =================================================================
# Master Thesis: Reaction strategies on Anomalous Events 
#				 in Computer Networks
# Author:        Sven Ossenbuehl 
# 			     Response Success Rate data object
# =================================================================

class rsr:
	def __init__(self, name, dpt, dps, rsr):
		# name of the resonse measure
		self.name = name
		# total number of deployments
		self.dpt  = dpt
		# number of successful deployments
		self.dps  = dps
		# response success rate
		self.rsr  = rsr
		
	def showinfo(self):
		print("name: " + str(self.name))
		print("dps: " + str(self.dps))
		print("dpt: " + str(self.dpt))
		print("rsr: " + str(self.rsr))

	def add_dps(self):
		self.dps = self.dps + 1.0

	def add_dpt(self):
		self.dpt = self.dpt + 1.0

	def update_rsr(self):
		if (self.dpt > 0):
			self.rsr = float(float(self.dps) / float(self.dpt))
		else:
			self.rsr = 0.0