# =================================================================
# Master Thesis: Reaction Strategies on Anomalous Events 
#				 in Computer Networks
# Author:        Sven Ossenbuehl 
# Script:	     Python script representing a helper class
# =================================================================
import sys, argparse 	# for parsing arguments
import commands 		# for system commands
import unified2.parser  # unified2 parser 
import socket, struct	# convert IP from str to int & vice versa
import datetime			
from time import strftime,strptime

# =================================================================
# GLOBALS, these paths might be change for other linux distros
# =================================================================

class InitiationModule:
	def __init__(self):

		self.clpath="/etc/nsm/snortBox-eth1/classification.config" 

		self.sigidpath="/etc/nsm/snortBox-eth1/sid-msg.map"

		self.scpath="service.config"

		self.rsppath="response.config"

		self.cldict = {0 : 'not used'}

		self.sigiddict={0 : 'Not used'}
		# service addresses (name -> ip)
		self.sadict = {'none': 'none'}
		# service importance low, medium, high
		self.scdict = {'none': 'none'}
		# disruptive impact of response
		self.rspdict= {'none' : 0.0}

	def initialize(self):
		# snort ids specific stuff
		self.define_classifications()
		self.define_sig_identifiers()

		# response selection configs.
		self.define_service_classifications()
		self.define_response_impact()

	def define_service_classifications(self):
		"""
		Reads the classification descriptions from (service.config) and stores it into the SC_DICT dictonary
		"""
		f = open(self.scpath, "rb")
		# early out if file is empty
		num_lines = sum(1 for line in f)
		f.close()
		if (num_lines == 0):
			print "[ERROR] service classiciation file is empty."
			exit(2)
		else:
			# work around (we have to open the file twice..)
			f = open(self.scpath, "rb")
			# todo: check hash
			for line in f:
				# skip comments
				if not '#' in  line[0]: 
					tmp        = line.split('|') 
					service    = tmp[0]
					importance = tmp[1]
					ipaddress  = tmp[2].strip()
					# add to dict
					if service not in self.scdict:
						self.scdict[service] = importance
					if service not in self.sadict:
						self.sadict[service] = ipaddress
			#print self.scdict
			#print self.sadict

	def define_response_impact(self):
		"""
		Reads the classification descriptions from (response.config) and stores it into the RSP_DICT dictonary
		"""
		f = open(self.rsppath, "rb")
		# early out if file is empty
		num_lines = sum(1 for line in f)
		f.close()
		if (num_lines == 0):
			print "[ERROR] response classiciation file is empty."
			exit(2)
		else:
			# work around (we have to open the file twice..)
			f = open(self.rsppath, "rb")
			# todo: check hash
			for line in f:
				if not '#' in line[0]:
					response = line.split('|')[0]
					importance = line.split('|')[1]
					self.rspdict[response] = float(importance.strip())		

	def define_classifications(self):
		"""
		Reads the classification descriptions from Snort (classification.config) and stores it into the CL_DICT dictonary
		"""

		f = open(self.clpath, "rb")
		# early out if file is empty
		num_lines = sum(1 for line in f)
		if (num_lines == 0):
			print "[ERROR] snort classiciation file is empty."
			exit(2)
		else:
			# work around (we have to open the file twice..)
			f = open(self.clpath, "rb")
			counter = 0
			for line in f:
				if counter not in self.cldict:
					self.cldict[counter] = line.split(',')[1]
				counter += 1	

	def define_sig_identifiers(self):
		"""
		Reads the signature descriptions from Snort (sid-msg.map) and stores it into the SIG_ID_DICT dictonary
		descriptions appear in the sid-msg.map if and only if the rule defintion contains a reference.
		"""
		f = open(self.sigidpath, "rb")
		# early out if file is empty
		num_lines = sum(1 for line in f)
		if (num_lines == 0):
			print "[ERROR] snort signature classiciation file is empty."
			exit(2)
		else:
			# work around (we have to open the file twice..)
			f = open(self.sigidpath, "rb")
			counter = 0
			for line in f:
				if counter not in self.sigiddict:
					tmp = line.split('||')
					sid = int(tmp[0])
					msg = line.split('||')[1].strip()
					self.sigiddict[sid] = msg
				counter += 1	

	def get_signature_descr(self, num):
		return self.sigiddict.get(num, "No description available.")

	def get_classification(self, num):
		return self.cldict.get(num, "No classification available.") 

	def get_service_importance(self, service_name):
		return self.scdict.get(str(service_name), "No classification available.")

	def get_service_address(self, service_name):
		return self.sadict.get(str(service_name), "No classification available.")

	def get_response_impact(self, response_name):
		return self.rspdict.get(response_name, "No classification available.")

	def get_arr_of_responses(self):
		arr = [] # list
		for key, value in self.rspdict.iteritems():
			arr.append(key)
		return arr

	# =================================================================
	# priority of the alert is related with the impact on the system.
	# =================================================================
	def get_highest_priority(self):
		# find the value for dividing in order to normalize
		# the priority for the classes are defined in: CL_PATH
		f = open(CL_PATH, "rb")
		# early out if file is empty
		num_lines = sum(1 for line in f)
		if (num_lines == 0):
			print "[ERROR] classiciation file is empty."
			exit(2)
		else:
			# work around (we have to open the file twice..)
			f = open(CL_PATH, "rb")
			counter = 0
			for line in f:
				priorty = int(line.split(',')[2])
				if counter < priorty:
					couter = priorty
			return priorty

# #Snort IDS specific
# CL_PATH		= "/etc/nsm/snortBox-eth1/classification.config" 
# CL_DICT 	= {0 : "Not used"}
# SIG_ID_PATH = "/etc/nsm/snortBox-eth1/sid-msg.map"
# SIG_ID_DICT = {0: "Not used"}

# #my stuff
# SC_PATH		= "service.config"
# SC_DICT		= {'None' : 'None'} # for service importance
# SA_DICT		= {'None' : 'None'} # fot service IP addresses

# RSP_PATH	= "response.config"
# RSP_DICT	= {'none' : 0.0}

# =================================================================
# Main Method
# =================================================================
def main():
	
	im = InitiationModule() 
	im.initialize()

if __name__ == "__main__":
	main()