# =================================================================
# Master Thesis: Reaction strategies on Anomalous Events 
#				 in Computer Networks
# Author:        Sven Ossenbuehl 
# Script:	     Python script to deploy responses
# =================================================================
import sys, argparse 				# for parsing arguments
import commands 					# for system commands
import datetime						# for time commands
from time import strftime,strptime	# for time commands

# Globals
LOGCHAIN_DROP	= "LOGNDROP"		# iptables CHAIN to log before drop
IP_TABLES_QUEUE = "FORWARD"			# the iptables CHAIN where
									# packets shall be discarded from
LOGCHAIN_LIMIT  = "LOGNLIMIT"		# iptables CHAIN to log before ratelimit

LOGCHAIN_DROP_PREFIX  = "BLOCKED" 	# log prefix to find in iptables log
LOGCHAIN_LIMIT_PREFIX = "LIMITED" 	# log prefix to find in iptables log 
# =================================================================
# Helper methods
# =================================================================
def add_minutes(tm, mins):
	return datetime.datetime.now() + datetime.timedelta(minutes=mins)

# =================================================================
# Responses
# =================================================================
def block_ip(ip_addr, ip_tables_queue, dstip):
	"""
	blocks an IP address permanently.
	"""	
	CMD = 'iptables -C '+str(ip_tables_queue)+' -s '+str(ip_addr)+' -d '+str(dstip)+' -j LOGNDROP'
	status, output = commands.getstatusoutput(CMD)
	if (status == 256):
		# if we end up here: the rule is not added yet, so it needs to be added
		CMD = 'iptables -I '+str(ip_tables_queue)+' -s '+str(ip_addr)+' -d '+str(dstip)+' -j LOGNDROP'
		status, output = commands.getstatusoutput(CMD)

# def block_ip_time(ip_addr, ip_tables_queue, num_min):
# 	"""
# 	blocks an IP address for a given amount of minutes.
# 	"""	

# 	datestart = datetime.datetime.now()
# 	datestop  = add_minutes(datestart, num_min)
	
# 	# iptables has a specific time format YYYY-mm-ddTHH:MM
# 	# thus we define it with strftime
# 	start 	  = datestart.strftime("%Y-%m-%dT%H:%M")
# 	stop  	  = datestop.strftime("%Y-%m-%dT%H:%M")

# 	# in test case just print
# 	# iptables -I FORWARD -s 141.100.1.26 -j DROP
# 	CMD = 'iptables -I ' + str(ip_tables_queue) + ' -s ' + str(ip_addr) + ' -m time --utc --datestart '	+ str(start) + ' --datestop ' + str(stop) + ' -j DROP'
# 	status, output = commands.getstatusoutput(CMD)

# def block_ip_port(ip_addr, port, num_min, ip_tables_queue):
# 	"""
# 	blocks an IP address for a given amount of minutes.
# 	"""	

# 	datestart = datetime.datetime.now()
# 	datestop  = add_minutes(datestart, num_min)
	
# 	# iptables has a specific time format YYYY-mm-ddTHH:MM
# 	# thus we define it with strftime
# 	start 	  = datestart.strftime("%Y-%m-%dT%H:%M")
# 	stop  	  = datestop.strftime("%Y-%m-%dT%H:%M")

# 	# in test case just print
# 	print 'iptables -I ' + str(ip_tables_queue) + ' -s ' + str(ip_address) + ' -m time --utc --datestart '	+ str(start) + ' --datestop ' + str(stop) + ' -j DROP'

def rate_limit_porto(protocol, ip_tables_queue,dstip):
	
	CMD = 'iptables -C'+str(ip_tables_queue)+' -p '+str(protocol)+'-s 141.100.1.0/24 '+str(dstip)+'  -m limit --limit 2/second --limit-burst 3 -j LOGNLIMIT'
	status, output = commands.getstatusoutput(CMD)
	
	if (status == 256):
		# if we end up here: the rule is not added yet, so it needs to be added
		CMD = 'iptables -I'+str(ip_tables_queue)+' -p '+str(protocol)+'-s 141.100.1.0/24 '+str(dstip)+' -m limit --limit 2/second --limit-burst 3 -j LOGNLIMIT'
		status, output = commands.getstatusoutput(CMD)

# =================================================================
# Main Method
# =================================================================
def main():

	parser = argparse.ArgumentParser(description='Response deployment module.') 
	parser.add_argument('--src',  		help='source IP:source Port')
	parser.add_argument('--dst',  		help='destination IP:destination Port')
	parser.add_argument('--response',	help='either blockip or ratelimit')
	parser.add_argument('--proto',		help='tcp, udp, icmp')
	parser.add_argument('--time', 		help='time in minutes for applying the measure')

	args = parser.parse_args()
   
	response = args.response   
	proto    = args.proto

	if (not (args.src is None)):	
		src 	 = args.src
		if (':' in src):
			srcIP    = src.split(':')[0]
			srcPort  = src.split(':')[1]
		else:
			srcIP    = src

	if (not (args.dst is None)):
		dst 	 = args.dst
		if (':' in dst):
			dstIP    = dst.split(':')[0]
			dstPort  = dst.split(':')[1]
		else:
			dstIP 	 = dst

	#time     = args.time
	#if (time <= 0):
	#	print "no time travel please specify a time > 0."
	#	exit(2)

	# MAIN:
	if 	 (response == "blockip"):
		block_ip(srcIP, IP_TABLES_QUEUE, dstIP)
	elif (response == "ratelimit"):
		rate_limit(proto, IP_TABLES_QUEUE, dstIP)
	else:
		print "not all arguments specified appropriately"
		exit(2)

if __name__ == "__main__":
	main()

