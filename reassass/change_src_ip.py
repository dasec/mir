#!/usr/bin/python
# =================================================================
# Master Thesis: Intrusion response strategies on anomalous events 
# Author:        Sven Ossenbuehl 
# Script:	     Python script to change src and dst ip addresses
#				 of a pcap file (requires scapy)
# =================================================================

import sys, logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
from scapy.all import *
from scapy.utils import rdpcap

# check inputs and early out
if (len(sys.argv) != 3):
	print "usage is ./change_src_ip.py infile.pcap outfile.pcap"
	sys.exit(1)	

infile  = sys.argv[1]
outfile = sys.argv[2]

try:
	pkts = rdpcap(infile)

	# delete IP and TCP checksums, in order to recreate them
	for p in pkts:
		if p.haslayer(IP):
			del p[IP].chksum
		if p.haslayer(TCP):
			del p[TCP].chksum

	# create dictionary
	ip_map = {}

	# define change source IP address 
	ip_map["192.168.101.2"] = "141.100.1.3"
	ip_map["192.168.101.1"] = "141.100.1.2"

	# define change source IP broadcast
	ip_map["192.168.101.255"] = "141.100.1.255"	

	# rewrite packets with new addresses:
	for p in pkts:
		if p.haslayer(IP):
			pl = p[IP]
			# change 192.168.101.2 => 141.100.1.3
			# or if not in ip_map leave it as is
			pl.src = ip_map.get(pl.src, pl.src)
			pl.dst = ip_map.get(pl.dst, pl.dst)

	# write packets to outputfile
	wrpcap(outfile, pkts)
except IOError:
	print "Failed reading file %s contents" % infile
	sys.exit(1)