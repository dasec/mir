#!/usr/bin/python
# =================================================================
# Master Thesis: Intrusion response strategies on anomalous events 
# Author:        Sven Ossenbuehl 
# Script:	     Response deployment module
#				 Python script to parse alert files from Snort
# =================================================================
import sys, getopt 	# for parsing arguments
import commands 	# for system commands
import subproces

import ap_mod		# test

mystring  = " hallo welth! "

print ap_mod.strip(mystring)