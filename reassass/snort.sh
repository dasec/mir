#/bin/bash
# =================================================================
# Master Thesis: Intrusion response strategies on anomalous events 
# Author:        Sven Ossenbühl 
# Script:	 script to install the snort IDS
#		 along with mysql, barnyard and snorby
# 		 this script will ask for  root privileges
# =================================================================

# =================================================================
# variables (for database)
# =================================================================
MYUSER=root
MYPASS=test
MYSERVER=localhost
MYDB=snorby
PW=inetsec

SNORTPATH=/usr/local/bin/snort
SNORTDIR=${HOME}/snort-2.9.6.0
SNORTRULES=${SNORTDIR}/rules
SNORTCONF=${SNORTDIR}/etc/snort.conf
RULES=snortrules-snapshot-2956.tar.gz

# oinkcode for pulledpork
OINKCODE=f4914b0cfeeb8f1fddb8703d744206bc1fbc4cfc

# =================================================================
# install dependencies
# =================================================================
echo "* downloading and installing dependencies.."
echo "* this may take some minutes..."
echo ${PW} | sudo -S apt-get install -y libpcap0.8 libpcap0.8-dev libdnet libdnet-dev libdumbnet-dev libdumbnet1 libpcre3 libpcre3-dev libdaq-dev libdaq0 zlib1g-dev flex bison build-essential checkinstall git libtool autoconf mysql-server libmysqld-dev ruby1.9.3 libxslt-dev libxml2-dev libmagickcore-dev libmagickwand-dev default-jre-headless autogen libwww-perl libssl-dev libcrypt-ssleay-perl 
#> /dev/null 2> error.log 
echo "* installing dependencies done."

# =================================================================
# get daq library (used by snort)
# =================================================================
echo "* downloading daq.."
wget --quiet http://www.snort.org/downloads/2778
echo "* downloading daq finished."

mv 2778 daq.tar.gz
tar -xf daq.tar.gz

cd daq-2.0.2
echo "* installing daq.."
./configure > /dev/null 2> error.log
make > /dev/null 2> error.log
echo ${PW} | sudo - S make install > /dev/null 2> error.log
cd -
echo "* installing daq done."

# =================================================================
# get snort
# =================================================================
echo "* downloading snort.."
wget --quiet  http://www.snort.org/downloads/2787
echo "* downloading snort finished."

mv 2787 snort.tar.gz
tar -xf snort.tar.gz
 
cd snort-2.9.6.0
echo "* installing snort..."

# =================================================================
# create symlinks and not created directories
# =================================================================
echo ${PW} | sudo -S mkdir -p /usr/local/etc/snort
echo ${PW} | sudo -S mkdir -p ${SNORTDIR}/rules

echo ${PW} | sudo -S ln -sf ${SNORTCONF}  /usr/local/etc/snort/snort.conf 
echo ${PW} | sudo -S ln -sf ${SNORTRULES} /usr/local/etc/snort/rules 

echo ${PW} | sudo -S cp ${SNORTDIR}/etc/*.* /usr/local/etc/snort/.

echo ${PW} | sudo -S mkdir -p /var/log/snort
echo ${PW} | sudo -S touch /var/log/snort/alert

# =================================================================
# --enable sourcefire was needed for barnyard2 and storing 
# things into mysql
# =================================================================
./configure --enable-sourcefire > /dev/null 2> error.log 
make > /dev/null 2> error.log 
echo ${PW} | sudo -S make install > /dev/null 2> error.log

echo "* configuring snort.."
echo ${PW} | sudo -S chmod 666 /usr/local/etc/snort/snort.conf
# not sure were the conf file does end up
echo ${PW} | sudo -S echo "output unified2: filename merged.log, limit 128" >> /usr/local/etc/snort/snort.conf 2> error.log
echo ${PW} | sudo -S echo "# for parsing with other tools:" >> /usr/local/etc/snort/snort.conf 2> error.log
echo ${PW} | sudo -S echo "output alert.fast: filename alert_fast, limit 128" >> /usr/local/etc/snort/snort.conf 2> error.log
echo "* installing snort done."

# =================================================================
# get barnyard2
# =================================================================
cd ${HOME}
echo "* downloading barnyard2..."
git clone -q https://github.com/firnsy/barnyard2.git
echo "* downloading barnyard2 done."

echo "* installing barnyard2..."
cd barnyard2
./autogen.sh > /dev/null 2> error.log
./configure --with-mysql --with-mysql-libraries=/usr/lib/x86_64-linux-gnu > /dev/null 2> error.log 
make > /dev/null 2> error.log 
echo ${PW} | sudo -S make install > /dev/null 2> error.log

echo "* configuring barnyard2.."
echo ${PW} | sudo -S chmod 666 /usr/local/etc/barnyard2.conf
echo ${PW} | sudo -S echo "output database: log, mysql, user=${MYUSER}, password=${MYPASS}, dbname=${MYDB} host=${MYSERVER}" >> /usr/local/etc/barnyard2.conf 2> error.log
echo "* installing barnyard2 done."

# =================================================================
# get snorby
# =================================================================
cd ${HOME}
echo "* downloading snorby..."
git clone -q http://github.com/Snorby/snorby.git
echo "* downloading snorby done"
echo ${PW} | sudo -S gem install bundler > /dev/null 2> error.log

cd snorby

cd config
mv snorby_config.yml.example snorby_config.yml
mv database.yml.example database.yml

# =================================================================
# configure snorby database.yml
# =================================================================
echo "* configure snorby..."

sed -i "s|username:.*|username: ${MYUSER}|g" database.yml
sed -i "s|password:.*|password: ${MYPASS}|g" database.yml
sed -i "s|host:.*|host: ${MYSERVER}|g" database.yml
sed -i "s|database:.*|database: ${MYDB}|g" database.yml

# =================================================================
# configure snorby config.yml
# =================================================================
sed -i "s|domain:.*|domain: localhost:3000|g" snorby_config.yml
#sed -i "s|wkhtmltopdf:.*|wkhtmltopdf: ${WKHTMLTOPDF}|g" #snorby_config.yml
#sed -i "s|mailer_sender:.*|mailer_sender: '${MAILTO}'|g" #snorby_config.yml
# add snorte rules
sed -i 's|  - ""|  - "/usr/local/etc/snort/rules"|g' snorby_config.yml
echo "* configuring snorby done."

cd -
echo "* installing snorby..."
bundle install > /dev/null 2> error.log
bundle exec rake snorby:setup  > /dev/null 2> error.log
echo "* istalling snorby done."

echo "* you can start the snorby rails webserver  by typing in the following command:"
echo "* bundle exec rails server -e production"

#bundle exec rails server -e production

# =================================================================
# adding rules
# =================================================================
#cd ${HOME}
#echo "* downloading community rules.."
#wget -q https://s3.amazonaws.com/snort-org/www/rules/community/community-rules.tar.gz
#echo "* downloading community rules done."
#echo "* adding community rules..."
##tar -xf community-rules.tar.gz
#cd community-rules
#echo ${PW} | sudo -S mv community.rules ${SNORTRULES}/community.rules
#echo ${PW} | sudo -S mv sid-msg.map ${SNORTDIR}/etc/sid-msg.map
#cd ${HOME}
#echo ${PW} | sudo -S mv ${RULES} ${SNORTDIR} 
#cd ${SNORTDIR}
#echo ${PW} | sudo -S tar -xf ${RULES}
#echo ${PW} | sudo -S chown inetsec:inetsec rules/*.rules
#echo "* adding community rules done."

# =================================================================
# we might need pulled pork
# =================================================================
cd ${HOME}
echo "* downloading pulledpork..."
wget -q http://pulledpork.googlecode.com/files/pulledpork-0.7.0.tar.gz -O pulledpork-0.7.0.tar.gz
echo "* downloading pulledpork done."

tar -xf pulledpork-0.7.0.tar.gz
cd pulledpork-0.7.0
./configure > /dev/null 2> error.log
make > /dev/null 2> error.log
echo ${PW} | sudo -S make install > /dev/null 2> error.log

# add rules dir
wget -q https://s3.amazonaws.com/snort-org/www/rules/community/community-rules.tar.gz
mv community-rules.tar.gz tmp/community-rules.tar.gz
# trick pulled pork
cp ${RULES} /tmp/snortrules-snapshot-2960.tar.gz

#echo "* add snort rules directory.."
#echo ${PW} | sudo -S mkdir -p ${SNORTDIR}/rules
#echo "* add snort rules directory done."

#echo "* downloading and adding snort rules.."
echo ${PW} | sudo -S chmod +x pulledpork.pl
sed -i "s/<oinkcode>/${OINKCODE}/g" etc/pulledpork.conf  
echo ${PW} | sudo -S perl -MCPAN  -e 'install Crypt::SSLeay'
perl pulledpork.pl -d -n -o /usr/local/etc/snort/rules  -c etc/pulledpork.conf -P

#echo "* downloading snort rules done."
#echo "* add snort rules done."

# =================================================================
# we might need tcpreplay
# =================================================================
cd ${HOME}
echo "* downloading tcpreplay..."
git clone -q https://github.com/appneta/tcpreplay.git
echo "* downloading tcpreplay done."
cd tcpreplay
echo "* installing tcpreplay.."
./autogen.sh > /dev/null 2> error.log
./configure > /dev/null 2> error.log
make > /dev/null 2> error.log
echo ${PW} | sudo -S make install > /dev/null 2> error.log
echo "* installing tcpreplay done."

# =================================================================
# clean house
# =================================================================
cd ${HOME}
echo "* cleaning up."
echo ${PW} | sudo -S rm community-rules.* daq.tar.gz snort.tar.gz pulledpork-0.7.0.tar.gz > /dev/null 2> error.log
echo ${PW} | sudo -S rm -rf community-rules
echo "* done."