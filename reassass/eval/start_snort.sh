# run snort with output mode fast for 
$SNORTCONF = "/etc/nsm/snortBox-eth1/snort.conf"
$INTERFACE = "eth1"

# -D for daemon / background modus
snort -c ${SNORTCONF} -i ${INTERFACE} -A fast -D

