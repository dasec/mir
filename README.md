# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
To overcome closed source and system dependency of the mitigation and response research domain , we share our scripts used by our communication process as well as the source code of our mitigation and response (MiR) system itself. 

### Where to look? ###
* [Wiki](https://bitbucket.org/dasec/mir/wiki/Home)
* [Known issues](https://bitbucket.org/dasec/mir/issues?status=new&status=open)
* [Downloads](https://bitbucket.org/dasec/mir/downloads)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions