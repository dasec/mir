# store the current directory
currentDir <- getwd()
# change to the new directory
setwd(currentDir)


#load libs
library("ggplot2")
library("scales")
library("grid")


#Funktion to layout multiple plots
multiplot <- function(..., plotlist=NULL, file, cols=1, layout=NULL) {
  
  # Make a list from the ... arguments and plotlist
  plots <- c(list(...), plotlist)
  
  numPlots = length(plots)
  
  # If layout is NULL, then use 'cols' to determine layout
  if (is.null(layout)) {
    # Make the panel
    # ncol: Number of columns of plots
    # nrow: Number of rows needed, calculated from # of cols
    layout <- matrix(seq(1, cols * ceiling(numPlots/cols)),
                     ncol = cols, nrow = ceiling(numPlots/cols))
  }
  
  if (numPlots==1) {
    print(plots[[1]])
    
  } else {
    # Set up the page
    grid.newpage()
    pushViewport(viewport(layout = grid.layout(nrow(layout), ncol(layout))))
    
    # Make each plot, in the correct location
    for (i in 1:numPlots) {
      # Get the i,j matrix positions of the regions that contain this subplot
      matchidx <- as.data.frame(which(layout == i, arr.ind = TRUE))
      
      print(plots[[i]], vp = viewport(layout.pos.row = matchidx$row,
                                      layout.pos.col = matchidx$col))
    }
  }
}

# load attack "dots"
aAttackDots <- read.csv("ifstatAsfDots.csv", sep=',')
aAttackDots$Total <- aAttackDots$eth4out
aAttackDots$Time <- as.POSIXct(aAttackDots$Time, format="%H:%M:%S")
aAttackDots <-transform(aAttackDots, prodAtt=aAttackDots$Total*0.001)

cAttackDots <- read.csv("ifstatCsfDots.csv", sep=',')
cAttackDots$Total <- cAttackDots$eth4out
cAttackDots$Time <- as.POSIXct(cAttackDots$Time, format="%H:%M:%S")
cAttackDots <-transform(cAttackDots, prodAtt=cAttackDots$Total*0.001)

# read files
a <- read.csv("ifstatA.csv", sep=';')
b <- read.csv("ifstatB.csv", sep=';')
c <- read.csv("ifstatC.csv", sep=';')
d <- read.csv("ifstatD.csv", sep=';')
e <- read.csv("ifstatE.csv", sep=';')

asf <- read.csv("ifstatAsf.csv", sep=',')
csf <- read.csv("ifstatCsf.csv", sep=',')

nodeA <- read.csv("nodeA.csv", sep=';')
nodeB <- read.csv("nodeB.csv", sep=';')
nodeC <- read.csv("nodeC.csv", sep=';')
nodeD <- read.csv("nodeD.csv", sep=';')
nodeE <- read.csv("nodeE.csv", sep=';')

totalA <- a$eth1out
totalB <- b$eth1in + b$eth3in + b$eth4in
totalC <- c$eth1out
totalD <- d$eth3in
totalE <- e$eth1in + e$eth4in
totalAsf <- asf$eth4out
totalCsf <- csf$eth4out

asf$total <- totalAsf
csf$total <- totalCsf
a$total <- totalA
b$total <- totalB
c$total <- totalC
d$total <- totalD
e$total <- totalE
e$totalAtt <- e$eth1in + e$eth1out + e$eth4in + e$eth4out

# calc node measurement timestamps
timeA<- as.POSIXct(a$Time, format="%H:%M:%S")
timeB<- as.POSIXct(b$Time, format="%H:%M:%S")
timeC<- as.POSIXct(c$Time, format="%H:%M:%S")
timeD<- as.POSIXct(d$Time, format="%H:%M:%S")
timeE<- as.POSIXct(e$Time, format="%H:%M:%S")
timeAsf<- as.POSIXct(asf$Time, format="%H:%M:%S")
timeCsf<- as.POSIXct(csf$Time, format="%H:%M:%S")

# calc node event timestamps
timeNodeA<-as.POSIXct(nodeA$Time, format="%H:%M:%S")
timeNodeB<-as.POSIXct(nodeB$Time, format="%H:%M:%S")
timeNodeC<-as.POSIXct(nodeC$Time, format="%H:%M:%S")
timeNodeD<-as.POSIXct(nodeD$Time, format="%H:%M:%S")
timeNodeE<-as.POSIXct(nodeE$Time, format="%H:%M:%S")

# set node measurement timestamps
a$timeA <- timeA
b$timeB <- timeB
c$timeC <- timeC
d$timeD <- timeD
e$timeE <- timeE
asf$timeAsf <- timeAsf
csf$timeCsf <- timeCsf

# set node event timestamps
nodeA$Time <-timeNodeA
nodeB$Time <-timeNodeB
nodeC$Time <-timeNodeC
nodeD$Time <-timeNodeD
nodeE$Time <-timeNodeE

# convert into Mbit/s
a <-transform(a, prod=a$total*0.001)
b <-transform(b, prod=b$total*0.001)
c <-transform(c, prod=c$total*0.001)
d <-transform(d, prod=d$total*0.001)
e <-transform(e, prod=e$total*0.001)
asf <-transform(asf, prod=asf$total*0.001)
csf <-transform(csf, prod=csf$total*0.001)
e <-transform(e, prodAtt=e$totalAtt*0.001)

#merge data frames
a$timestamp<-  strftime(a$timeA,"%Y-%m-%d %H:%M:%S")
nodeA$timestamp<-  strftime(nodeA$Time,"%Y-%m-%d %H:%M:%S")
newNodeA <- merge(nodeA, a, "timestamp")
#remove duplicates
newNodeA <- newNodeA[!duplicated(newNodeA),]

b$timestamp<-  strftime(b$timeB,"%Y-%m-%d %H:%M:%S")
nodeB$timestamp<-  strftime(nodeB$Time,"%Y-%m-%d %H:%M:%S")
newNodeB <- merge(nodeB, b, "timestamp")
#remove duplicates
newNodeB <- newNodeB[!duplicated(newNodeB),]

c$timestamp<-  strftime(c$timeC,"%Y-%m-%d %H:%M:%S")
nodeC$timestamp<-  strftime(nodeC$Time,"%Y-%m-%d %H:%M:%S")
newNodeC <- merge(nodeC, c, "timestamp")
#remove duplicates
newNodeC <- newNodeC[!duplicated(newNodeC),]

d$timestamp<-  strftime(d$timeD,"%Y-%m-%d %H:%M:%S")
nodeD$timestamp<-  strftime(nodeD$Time,"%Y-%m-%d %H:%M:%S")
newNodeD <- merge(nodeD, d, "timestamp")
#remove duplicates
newNodeD <- newNodeD[!duplicated(newNodeD),]


e$timestamp<-  strftime(e$timeE,"%Y-%m-%d %H:%M:%S")
nodeE$timestamp<-  strftime(nodeE$Time,"%Y-%m-%d %H:%M:%S")
newNodeE <- merge(nodeE, e, "timestamp")
newNodeE <- newNodeE[!duplicated(newNodeE),]

# limits x-axis
lims <- as.POSIXct(strptime(c("05:05:20","05:05:55"), format = "%H:%M:%S"))
limsMessageFlow <- as.POSIXct(strptime(c("05:05:43","05:05:50"), format = "%H:%M:%S"))


# plot
p1 <- ggplot()
p1 <- p1+geom_line(data=a, aes(x=timeA, y=prod, group=1), size=.5)
p1 <- p1+geom_line(data=asf, aes(x=timeAsf, y=prod, group=1), size=0.5, colour='red') # attack
p1 <- p1+geom_point(data=newNodeA, aes(x=timeA, y=prod, group=1, color=factor(Event), shape=Event), size=4)
p1 <- p1+geom_line(data=aAttackDots, aes(x=aAttackDots$Time, y=prodAtt, group=1), size=.5, color='red', linetype="dashed") # attack
p1 <- p1 + ylab("Average network traffic [Mb/s]")
p1 <- p1 + labs(color="Event")
p1 <- p1 + xlab('Time')
p1 <- p1 + theme(axis.text.x=element_text(face="bold", size=8))
p1 <- p1 + theme(axis.text.y=element_text(face="bold", size=12))
p1 <- p1 + theme(axis.title.y=element_text(vjust=1.5,face="bold", size=14))
p1 <- p1 + theme(axis.title.x=element_text(hjust=0.5,face="bold", size=14))
p1 <- p1 + theme(legend.title = element_text(size=9, face="bold"))
p1 <- p1 + theme(legend.text = element_text(size=9,  face="bold"))
p1 <- p1 + theme(legend.background = element_rect(fill=alpha('white', 0.4)))
p1 <- p1 + theme(legend.position = c(0.25, 0.69))
p1 <- p1 + theme(panel.background = element_rect(fill='white', colour='black'))
p1 <- p1 + theme(panel.grid.major=element_line(color="grey"))
p1 <- p1 + theme(panel.grid.major=element_line(color="black", linetype = "dotted"))
p1 <- p1 + expand_limits(y=c(0, 400))
p1 <- p1 + ggtitle("ISP A")
p1 <- p1 + scale_x_datetime(limits=lims,labels=date_format("%H:%M:%S"))
p1 <- p1 + scale_colour_brewer(palette="Set1")
p1 <- p1 + scale_shape_manual(values=c(0,8,18,6))



p2 <- ggplot()
p2 <- p2+geom_line(data=b, aes(x=timeB, y=prod, group=1), size=.5)
p2 <- p2+geom_point(data=newNodeB, aes(x=timeB, y=prod, group=1, color=factor(Event),shape=Event), size=4)
p2 <- p2 + ylab("")
p2 <- p2 + labs(color="Event")
p2 <- p2 + xlab('Time')
p2 <- p2 + theme(axis.text.x=element_text(face="bold", size=8))
p2 <- p2 + theme(axis.text.y=element_text(face="bold", size=12))
p2 <- p2 + theme(axis.title.y=element_text(vjust=1.5,face="bold", size=14))
p2 <- p2 + theme(axis.title.x=element_text(hjust=0.5,face="bold", size=14))
p2 <- p2 + theme(legend.title = element_text(size=10, face="bold"))
p2 <- p2 + theme(legend.text = element_text(size=10,  face="bold"))
p2 <- p2 + theme(legend.position = c(0.17, 0.8))
p2 <- p2 + theme(legend.background = element_rect(fill=alpha('white', 0.4)))
p2 <- p2 + theme(panel.background = element_rect(fill='white', colour='black'))
p2 <- p2 + theme(panel.grid.major=element_line(color="grey"))
#p2 <- p2 + scale_x_datetime(labels= date_format("%H:%M:%S"))
p2 <- p2 + theme(panel.grid.major=element_line(color="black", linetype = "dotted"))
p2 <- p2 + expand_limits(y=c(0, 400))
p2 <- p2 + ggtitle("ISP B")
p2 <-  p2 + scale_x_datetime(limits=lims,labels=date_format("%H:%M:%S"))
p2 <- p2 + scale_colour_brewer(palette="Set1")
p2 <- p2 + scale_shape_manual(values=c(0,8,18,4))


p3 <- ggplot()
p3 <- p3+geom_line(data=c, aes(x=timeC, y=prod, group=1), size=.5)
p3 <- p3+geom_line(data=csf, aes(x=timeCsf, y=prod, group=1), size=0.5, colour='red') # attack
p3 <- p3+geom_line(data=cAttackDots, aes(x=cAttackDots$Time, y=prodAtt, group=1), size=.5, color='red', linetype="dashed") # attack
p3 <- p3+geom_point(data=newNodeC, aes(x=timeC, y=prod, group=1, color=factor(Event),shape=Event), size=4)
p3 <- p3 + ylab("")
p3 <- p3 + labs(color="Event")
p3 <- p3 + xlab('Time')
p3 <- p3 + theme(axis.text.x=element_text(face="bold", size=8))
p3 <- p3 + theme(axis.text.y=element_text(face="bold", size=12))
p3 <- p3 + theme(axis.title.y=element_text(vjust=1.5,face="bold", size=14))
p3 <- p3 + theme(axis.title.x=element_text(hjust=0.5,face="bold", size=14))
p3 <- p3 + theme(legend.title = element_text(size=10, face="bold"))
p3 <- p3 + theme(legend.text = element_text(size=10,  face="bold"))
p3 <- p3 + theme(legend.background = element_rect(fill=alpha('white', 0.4)))
p3 <- p3 + theme(legend.position = c(0.17, 0.8))
p3 <- p3 + theme(panel.background = element_rect(fill='white', colour='black'))
p3 <- p3 + theme(panel.grid.major=element_line(color="grey"))
#p3 <- p3 + scale_x_datetime(labels= date_format("%H:%M:%S"))
p3 <- p3 + theme(panel.grid.major=element_line(color="black", linetype = "dotted"))
p3 <- p3 + expand_limits(y=c(0, 400))
p3 <- p3 + ggtitle("ISP C")
p3 <- p3 + scale_x_datetime(limits=lims,labels=date_format("%H:%M:%S"))
p3 <- p3 + scale_colour_brewer(palette="Set1")
p3 <- p3 + scale_shape_manual(values=c(0,8,18,4))


p4 <- ggplot()
p4 <- p4+geom_line(data=d, aes(x=timeD, y=prod, group=1), size=.5)
p4 <- p4+geom_point(data=newNodeD, aes(x=timeD, y=prod, group=1, color=factor(Event),shape=Event), size=4)
p4 <- p4 + ylab("Average network traffic [Mb/s]")
p4 <- p4 + labs(color="Event")
p4 <- p4 + xlab('Time')
p4 <- p4 + theme(axis.text.x=element_text(face="bold", size=8))
p4 <- p4 + theme(axis.text.y=element_text(face="bold", size=12))
p4 <- p4 + theme(axis.title.y=element_text(vjust=1.5,face="bold", size=14))
p4 <- p4 + theme(axis.title.x=element_text(hjust=0.5,face="bold", size=14))
p4 <- p4 + theme(legend.title = element_text(size=10, face="bold"))
p4 <- p4 + theme(legend.text = element_text(size=10,  face="bold"))
p4 <- p4 + theme(legend.background = element_rect(fill=alpha('white', 0.4)))
p4 <- p4 + theme(legend.position = c(0.17, 0.8))
p4 <- p4 + theme(panel.background = element_rect(fill='white', colour='black'))
p4 <- p4 + theme(panel.grid.major=element_line(color="grey"))
#p4 <- p4 + scale_x_datetime(labels= date_format("%H:%M:%S"))
p4 <- p4 + theme(panel.grid.major=element_line(color="black", linetype = "dotted"))
p4 <- p4 + expand_limits(y=c(0, 400))
p4 <- p4 + ggtitle("ISP D")
p4 <- p4 + scale_x_datetime(limits=lims,labels=date_format("%H:%M:%S"))
p4 <- p4 + scale_colour_brewer(palette="Set1")
p4 <- p4 + scale_shape_manual(values=c(0,8,18,4))


p5 <- ggplot()
p5 <- p5+geom_line(data=e, aes(x=timeE, y=prodAtt, group=1), size=0.5, colour='black') # attack
p5 <- p5+geom_point(data=newNodeE, aes(x=timeE, y=prodAtt, group=1, color=factor(Event),shape=Event), size=4)
p5 <- p5 + ylab("")
p5 <- p5 + labs(color="Event")
p5 <- p5 + xlab('Time')
p5 <- p5 + theme(axis.text.x=element_text(face="bold", size=8))
p5 <- p5 + theme(axis.text.y=element_text(face="bold", size=12))
p5 <- p5 + theme(axis.title.y=element_text(vjust=1.5,face="bold", size=14))
p5 <- p5 + theme(axis.title.x=element_text(hjust=0.5,face="bold", size=14))
p5 <- p5 + theme(legend.title = element_text(size=10, face="bold"))
p5 <- p5 + theme(legend.text = element_text(size=10, face="bold"))
p5 <- p5 + theme(legend.background = element_rect(fill=alpha('white', 0.4)))
p5 <- p5 + theme(legend.position = c(0.17, 0.8))
p5 <- p5 + theme(panel.background = element_rect(fill='white', colour='black'))
p5 <- p5 + theme(panel.grid.major=element_line(color="grey"))
#p5 <- p5 + scale_x_datetime(labels= date_format("%H:%M:%S"))
p5 <- p5 + theme(panel.grid.major=element_line(color="black", linetype = "dotted"))
p5 <- p5 + expand_limits(y=c(0, 400))
p5 <- p5 + ggtitle("ISP E")
p5 <- p5 + scale_x_datetime(limits=lims,labels=date_format("%H:%M:%S"))
p5 <- p5 + scale_colour_brewer(palette="Set1")
p5 <- p5 + scale_shape_manual(values=c(0,8,18,4))


# read message flow files
comA <- read.csv("nodeA.csv", sep=';')
comB <- read.csv("nodeB.csv", sep=';')
comC <- read.csv("nodeC.csv", sep=';')
comD <- read.csv("nodeD.csv", sep=';')
comE <- read.csv("nodeE.csv", sep=';')

com <- rbind(comA, comB, comC, comD, comE )
com$Time<- as.POSIXct(com$Time, format="%H:%M:%S")
 
# read arrows
a1a2 <- read.csv("flowArrows/a1a2.csv", sep=',')
a1a2$Time <- as.POSIXct(a1a2$Time, format="%H:%M:%S")

a2a3 <- read.csv("flowArrows/a2a3.csv", sep=',')
a2a3$Time <- as.POSIXct(a2a3$Time, format="%H:%M:%S")

a2b1 <- read.csv("flowArrows/a2b1.csv", sep=',')
a2b1$Time <- as.POSIXct(a2b1$Time, format="%H:%M:%S")

a2d1 <- read.csv("flowArrows/a2d1.csv", sep=',')
a2d1$Time <- as.POSIXct(a2d1$Time, format="%H:%M:%S")

a3d1 <- read.csv("flowArrows/a3d1.csv", sep=',')
a3d1$Time <- as.POSIXct(a3d1$Time, format="%H:%M:%S")

a3b1 <- read.csv("flowArrows/a3b1.csv", sep=',')
a3b1$Time <- as.POSIXct(a3b1$Time, format="%H:%M:%S")

b1a4 <- read.csv("flowArrows/b1a4.csv", sep=',')
b1a4$Time <- as.POSIXct(b1a4$Time, format="%H:%M:%S")

b1d2 <- read.csv("flowArrows/b1d2.csv", sep=',')
b1d2$Time <- as.POSIXct(b1d2$Time, format="%H:%M:%S")

d1a5 <- read.csv("flowArrows/d1a5.csv", sep=',')
d1a5$Time <- as.POSIXct(d1a5$Time, format="%H:%M:%S")

d1b2 <- read.csv("flowArrows/d1b2.csv", sep=',')
d1b2$Time <- as.POSIXct(d1b2$Time, format="%H:%M:%S")

c1b3 <- read.csv("flowArrows/c1b3.csv", sep=',')
c1b3$Time <- as.POSIXct(c1b3$Time, format="%H:%M:%S")

b1c1 <- read.csv("flowArrows/b1c1.csv", sep=',')
b1c1$Time <- as.POSIXct(b1c1$Time, format="%H:%M:%S")

e1c2 <- read.csv("flowArrows/e1c2.csv", sep=',')
e1c2$Time <- as.POSIXct(e1c2$Time, format="%H:%M:%S")

e1d3 <- read.csv("flowArrows/e1d3.csv", sep=',')
e1d3$Time <- as.POSIXct(e1d3$Time, format="%H:%M:%S")

d1e1 <- read.csv("flowArrows/d1e1.csv", sep=',')
d1e1$Time <- as.POSIXct(d1e1$Time, format="%H:%M:%S")

c1e2 <- read.csv("flowArrows/c1e2.csv", sep=',')
c1e2$Time <- as.POSIXct(c1e2$Time, format="%H:%M:%S")

p7 <- ggplot()

plotColA <- "#294273"
plotColB <- "#206e8c"
plotColC <- "#2da690"
plotColD <- "#f29441"
plotColE <- "#d94b2b"

arrowLength <- 5

p7 <- p7 + geom_line(data=a1a2, aes(x=Time, y=Receiver, group=1), size=.5, arrow = arrow(angle = 15,type = "closed", length = unit(arrowLength,"mm")),colour=plotColA)
p7 <- p7 + geom_line(data=a2a3, aes(x=Time, y=Receiver, group=1), size=.5, arrow = arrow(angle = 15,type = "closed", length = unit(arrowLength,"mm")),colour=plotColA)
p7 <- p7 + geom_line(data=a2d1, aes(x=Time, y=Receiver, group=1), size=.5, arrow = arrow(angle = 15,type = "closed", length = unit(arrowLength,"mm")),colour=plotColA)
p7 <- p7 + geom_line(data=a2b1, aes(x=Time, y=Receiver, group=1), size=.5, arrow = arrow(angle = 15,type = "closed", length = unit(arrowLength,"mm")),colour=plotColA)
p7 <- p7 + geom_line(data=a3d1, aes(x=Time, y=Receiver, group=1), size=.5, arrow = arrow(angle = 15,type = "closed", length = unit(arrowLength,"mm")),colour=plotColA)
p7 <- p7 + geom_line(data=a3b1, aes(x=Time, y=Receiver, group=1), size=.5, arrow = arrow(angle = 15,type = "closed", length = unit(arrowLength,"mm")),colour=plotColA)

p7 <- p7 + geom_line(data=b1a4, aes(x=Time, y=Receiver, group=1), size=.5, arrow = arrow(angle = 15,type = "closed", length = unit(arrowLength,"mm")),colour=plotColB)
p7 <- p7 + geom_line(data=b1d2, aes(x=Time, y=Receiver, group=1), size=.5, arrow = arrow(angle = 15,type = "closed", length = unit(arrowLength,"mm")),colour=plotColB)
p7 <- p7 + geom_line(data=b1c1, aes(x=Time, y=Receiver, group=1), size=.5, arrow = arrow(angle = 15,type = "closed", length = unit(arrowLength,"mm")),colour=plotColB)

p7 <- p7 + geom_line(data=c1b3, aes(x=Time, y=Receiver, group=1), size=.5, arrow = arrow(angle = 15,type = "closed", length = unit(arrowLength,"mm")),colour=plotColC)
p7 <- p7 + geom_line(data=c1e2, aes(x=Time, y=Receiver, group=1), size=.5, arrow = arrow(angle = 15,type = "closed", length = unit(arrowLength,"mm")),colour=plotColC)
 
p7 <- p7 + geom_line(data=d1a5, aes(x=Time, y=Receiver, group=1), size=.5, arrow = arrow(angle = 15,type = "closed", length = unit(arrowLength,"mm")),colour=plotColD)
p7 <- p7 + geom_line(data=d1b2, aes(x=Time, y=Receiver, group=1), size=.5, arrow = arrow(angle = 15,type = "closed", length = unit(arrowLength,"mm")),colour=plotColD)
p7 <- p7 + geom_line(data=d1e1, aes(x=Time, y=Receiver, group=1), size=.5, arrow = arrow(angle = 15,type = "closed", length = unit(arrowLength,"mm")),colour=plotColD)

p7 <- p7 + geom_line(data=e1c2, aes(x=Time, y=Receiver, group=1), size=.5, arrow = arrow(angle = 15,type = "closed", length = unit(arrowLength,"mm")),colour=plotColE)
p7 <- p7 + geom_line(data=e1d3, aes(x=Time, y=Receiver, group=1), size=.5, arrow = arrow(angle = 15,type = "closed", length = unit(arrowLength,"mm")),colour=plotColE)

p7 <- p7 + geom_point(data=com, aes(x=Time, y=Receiver, group=1, colour=Event, shape=Event), size=4)
#p7 <- p7 + facet_wrap(~IP, ncol=1)
p7 <- p7 + ylab("ISP")
p7 <- p7 + xlab('Time')
p7 <- p7 + theme(panel.background = element_rect(fill='white', colour='black'))
p7 <- p7 + theme(panel.grid.major=element_line(color="grey"))
p7 <- p7 + scale_colour_brewer("Event", palette="Set1")
p7 <- p7 + scale_shape_manual("Event", values=c(15,8,18,6))
p7 <- p7 + scale_x_datetime(limits=limsMessageFlow,labels= date_format("%H:%M:%S"))
p7 <- p7 + theme(axis.text.x=element_text(face="bold", size=8))
p7 <- p7 + theme(axis.text.y=element_text(face="bold", size=12))
p7 <- p7 + theme(axis.title.y=element_text(vjust=1.5,face="bold", size=14))
p7 <- p7 + theme(axis.title.x=element_text(hjust=0.5,face="bold", size=14))
p7 <- p7 + theme(legend.title = element_text(size=9, face="bold"))
p7 <- p7 + theme(legend.text = element_text(size=9, face="bold"))
p7 <- p7 + theme(legend.background = element_rect(fill=alpha('white', 0.4)))
p7 <- p7 + theme(legend.position = c(0.25, 0.69))
p7 <- p7 + ggtitle("Message flow")

# Ordering related to paper schematic presenation
multiplot(p1, p4, p2, p5, p3, p7, cols=3)
